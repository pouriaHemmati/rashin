package org.pouria.rashin.app.adapter;


import org.pouria.rashin.model.JGetContentList;
import org.pouria.rashin.model.JResponse;

import java.util.ArrayList;

public class ModelItemSearch {


    ArrayList<JGetContentList> jResponse;

    public ModelItemSearch(ArrayList<JGetContentList> jResponse) {
        this.jResponse = jResponse;

    }


    public void setResponse(ArrayList<JGetContentList> jResponse) {
        this.jResponse = jResponse;
    }

    public ArrayList<JGetContentList> getResponse() {
        return jResponse;
    }
}


