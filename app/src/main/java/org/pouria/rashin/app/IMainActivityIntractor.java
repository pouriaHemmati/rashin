package org.pouria.rashin.app;


import org.pouria.rashin.model.JResponse;
import org.pouria.rashin.model.JResult;

public interface IMainActivityIntractor {
    void Person(String PageIndex , int zoneID, IPersonIFinishedListener iPersonIFineshedListener);
    interface IPersonIFinishedListener{
        void successPerson(JResponse jResult);
        void errorPerson(String error);
    }
}
