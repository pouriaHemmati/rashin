package org.pouria.rashin.utilis;


import org.pouria.rashin.model.JGetContentList;
import org.pouria.rashin.model.JResponse;

public class SearchModelSend {
    private JGetContentList jGetContentList;

    private static SearchModelSend sreachModelSendInstance = null;

    public static SearchModelSend getSreachModelSendInstance() {
        if (sreachModelSendInstance == null)
            sreachModelSendInstance = new SearchModelSend();
        return sreachModelSendInstance;
    }

    public JGetContentList getResponse() {
        return jGetContentList;
    }

    public void setResponse(JGetContentList jGetContentList) {
        this.jGetContentList = jGetContentList;
    }

    public void loadResponse() {
        this.jGetContentList = new JGetContentList();
    }
}

