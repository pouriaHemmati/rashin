package org.pouria.rashin.app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.pouria.rashin.R;
import org.pouria.rashin.base.BaseActivity;
import org.pouria.rashin.model.JGetContentList;
import org.pouria.rashin.model.JResponse;

import java.util.ArrayList;


public class SearchRecyclerAdapter extends RecyclerView.Adapter<SearchRecyclerAdapter.ViewHolder> {
    private ModelItemSearch requestItems;
    private IOnClickMovie iOnClickMovie;
    private Context context;
    private Picasso picasso;


    public SearchRecyclerAdapter(Context context, ModelItemSearch requestItems , IOnClickMovie iOnClickMovie ) {
        this.context = context;
        this.requestItems = requestItems;
        this.iOnClickMovie = iOnClickMovie;


    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

       public ImageView logo_recycler;
       public TextView txt_title_recycler;
       public TextView txt_zoneID_recycler;
       public CardView layout_search;





        public ViewHolder(View itemView) {
            super(itemView);

            logo_recycler = itemView.findViewById(R.id.logo_recycler);
            txt_title_recycler = itemView.findViewById(R.id.txt_title_recycler);
            txt_zoneID_recycler = itemView.findViewById(R.id.txt_zoneID_recycler);
            layout_search = itemView.findViewById(R.id.layout_search);
            picasso = Picasso.with(BaseActivity.getContext());



        }

        @Override
        public void onClick(View v) {

        }


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recycler_search, parent, false);
        return new ViewHolder(view);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ArrayList<JGetContentList> jGetContentLists = requestItems.jResponse;

        if (jGetContentLists.get(position).ThumbImage !=null){
            picasso.load(jGetContentLists.get(position).ThumbImage)
                    .into(holder.logo_recycler);
        }

        if (jGetContentLists.get(position).Title !=null)
        holder.txt_title_recycler.setText(jGetContentLists.get(position).Title);


        if (jGetContentLists.get(position).ZoneID > -1){
             if (jGetContentLists.get(position).ZoneID == 3){
                 holder.txt_zoneID_recycler.setText(R.string.ZoneId3);
            }else  if (jGetContentLists.get(position).ZoneID == 4){
                 holder.txt_zoneID_recycler.setText(R.string.ZoneId4);
            } else {
                 holder.txt_zoneID_recycler.setText(R.string.ZoneId);
             }
        }

        holder.layout_search.setOnClickListener(v ->{
            iOnClickMovie.onClickMovie(jGetContentLists.get(position));
        });


    }


    @Override
    public int getItemCount() {
        return requestItems.jResponse.size();
    }


}