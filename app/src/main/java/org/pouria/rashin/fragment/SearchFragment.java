package org.pouria.rashin.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.pouria.rashin.R;
import org.pouria.rashin.app.IMainActivityIntractor;
import org.pouria.rashin.app.MainActivityIntractor;
import org.pouria.rashin.app.adapter.IOnClickMovie;
import org.pouria.rashin.app.adapter.ModelItemSearch;
import org.pouria.rashin.app.adapter.SearchRecyclerAdapter;
import org.pouria.rashin.base.BaseActivity;
import org.pouria.rashin.model.JGetContentList;
import org.pouria.rashin.model.JResponse;
import org.pouria.rashin.utilis.SearchModelSend;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchFragment extends Fragment implements IOnClickMovie {

    private boolean FirstSearch = true;
    IMainActivityIntractor iMainActivityIntractor;
    private SearchModelSend searchModelSend;
    private ArrayList<JGetContentList> jGetContentListArrayList;
    private JResponse jResponse;
    private int paginate = 0;
    ModelItemSearch modelItemSearch;
    SearchRecyclerAdapter searchRecyclerAdapter;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.layout_loading)
    RelativeLayout layout_loading;
    @BindView(R.id.radios)
    RadioGroup radios;
    @BindView(R.id.radio_all)
    RadioButton radio_all;
    @BindView(R.id.radio_serial)
    RadioButton radio_serial;
    @BindView(R.id.radio_cinematic)
    RadioButton radio_cinematic;
    View view;
    private View bottomSheet;
    private BottomSheetBehavior<View> behavior;
    private int zoneId = 1;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);

        // note
        CoordinatorLayout coordinatorLayoutIncome = (CoordinatorLayout) view.findViewById(R.id.income_content);
        bottomSheet = coordinatorLayoutIncome.findViewById(R.id.text_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);

        radios.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.radio_all:
                        zoneId = 1;
                        paginate = 0;
                        FirstSearch = true;
                        request(paginate , zoneId);
                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        layout_loading.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radio_serial:
                        zoneId = 3;
                        paginate = 0;
                        FirstSearch = true;
                        request(paginate , zoneId);
                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        layout_loading.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radio_cinematic:
                        zoneId = 4;
                        paginate = 0;
                        FirstSearch = true;
                        request(paginate , zoneId);
                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        layout_loading.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // setting model
        searchModelSend = SearchModelSend.getSreachModelSendInstance();

        // set request setting
        iMainActivityIntractor = new MainActivityIntractor();
        request(paginate , zoneId);

        // recycler paginate
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    layout_loading.setVisibility(View.VISIBLE);
                    paginate = paginate + 1;
                    request(paginate , zoneId);

                }
            }
        });



    }


    // request
    private void request(int paginates , int zoneId) {
        iMainActivityIntractor.Person(String.valueOf(paginates) , zoneId , new IMainActivityIntractor.IPersonIFinishedListener() {
            @Override
            public void successPerson(JResponse jResult) {

                if (FirstSearch) {
                    jGetContentListArrayList = jResult.Result.GetContentList;
                }
                jResponse = jResult;

                recyclerSearch();
                layout_loading.setVisibility(View.GONE);

            }

            @Override
            public void errorPerson(String error) {

            }
        });
    }

    @OnClick(R.id.btn_filter)
    void onTextClick() {
        if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else  if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

    }
    @OnClick(R.id.img_close_filter)
    void img_close_filter() {
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    // recycler setting
    private void recyclerSearch() {
        if (!FirstSearch) {
            this.jGetContentListArrayList.addAll(jResponse.Result.GetContentList);
            searchRecyclerAdapter.notifyDataSetChanged();

        } else {
            modelItemSearch = new ModelItemSearch(jResponse.Result.GetContentList);
            searchRecyclerAdapter = new SearchRecyclerAdapter(BaseActivity.getContext(), modelItemSearch, this);
            recycler.setLayoutManager(new LinearLayoutManager(BaseActivity.getContext(), LinearLayoutManager.VERTICAL, false));
            recycler.setAdapter(searchRecyclerAdapter);
            FirstSearch = false;
        }


    }

    // click listener recycler
    @Override
    public void onClickMovie(JGetContentList jGetContentList) {
        // set model
        searchModelSend.loadResponse();
        searchModelSend.setResponse(jGetContentList);

        startActivity(DetailsActivity.class);

    }

    public void startActivity(Class<?> otherActivityClass) {
        Intent intent = new Intent(BaseActivity.getContext(), otherActivityClass);
        startActivity(intent);
    }
}
