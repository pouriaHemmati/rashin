package org.pouria.rashin.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDexApplication;


import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class BaseActivity extends MultiDexApplication {

    private static BaseActivity instance = null;
    private static Context context;
    private static Activity currentActivity;
    private static Handler handler;
    private static LayoutInflater layoutInflater;
    private static TransitionInflater transitionInflater;
    public static final String TAG = BaseActivity.class.getSimpleName();


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();


    }








    public static Context getContext() {
        if (currentActivity != null) {
            return currentActivity;
        }
        return context;
    }

    public static Context getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(Activity activity) {
        currentActivity = activity;
    }

    public static synchronized BaseActivity getInstance() {
        if (instance == null) {
            instance = new BaseActivity();
        }
        return instance;
    }


    public static Handler getHandler() {
        return handler;
    }

    public static LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public static TransitionInflater getTransitionInflater() {
        return transitionInflater;
    }

    @SuppressLint("ResourceType")
    public static Transition transition(@LayoutRes int res) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return transitionInflater.inflateTransition(res);
        }
        return null;
    }

    public static View inflateLayout(@LayoutRes int res) {
        return layoutInflater.inflate(res, null);
    }

    public static View inflateLayout(@LayoutRes int res, @Nullable ViewGroup root) {
        return layoutInflater.inflate(res, root);
    }



}
