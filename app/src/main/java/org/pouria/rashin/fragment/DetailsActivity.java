package org.pouria.rashin.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import com.squareup.picasso.Picasso;

import org.pouria.rashin.R;
import org.pouria.rashin.base.BaseActivity;
import org.pouria.rashin.base.BaseAppCompatActivity;
import org.pouria.rashin.model.JGetContentList;
import org.pouria.rashin.utilis.SearchModelSend;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends BaseAppCompatActivity {

    @BindView(R.id.header)
    ImageView picture;
    @BindView(R.id.txt_title_details)
    TextView txt_title_details;
    @BindView(R.id.txt_Summary_details)
    TextView txt_Summary_details;
    Picasso picasso;
    SearchModelSend searchModelSend;
    JGetContentList jGetContentList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_details);
        ButterKnife.bind(this);
        picasso = Picasso.with(BaseActivity.getContext());
        searchModelSend = SearchModelSend.getSreachModelSendInstance();
    }







    @Override
    public void onResume() {
        super.onResume();
        jGetContentList = searchModelSend.getResponse();
        picasso.load(jGetContentList.ThumbImage)
                .into(picture);
        txt_title_details.setText(jGetContentList.Title);
        txt_Summary_details.setText(jGetContentList.Summary);

    }


   @OnClick(R.id.img_back)
    public void img_back(){
        finish();
   }




}
