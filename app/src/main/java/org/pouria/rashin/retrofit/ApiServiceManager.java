package org.pouria.rashin.retrofit;


import org.pouria.rashin.utilis.Urls;

public class ApiServiceManager {

    public static ApiService getBaseUrl() {
        return RetrofitClient.getRetrofit(Urls.BASE_URL,"BaseUrl").create(ApiService.class);
    }
}
