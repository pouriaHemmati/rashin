package org.pouria.rashin.app;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;
import org.pouria.rashin.model.JResponse;
import org.pouria.rashin.model.JResult;
import org.pouria.rashin.retrofit.ApiService;
import org.pouria.rashin.retrofit.ApiServiceManager;
import org.pouria.rashin.utilis.MyGson;


import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class MainActivityIntractor implements IMainActivityIntractor {
    @Override
    public void Person(String PageIndex , int zoneId , IPersonIFinishedListener iPersonIFinishedListener) {
        ApiService apiService = ApiServiceManager.getBaseUrl();
        JsonObject request = new JsonObject();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("RequestType", "2");
        jsonObject.addProperty("orderby", "StartShowDate");
        jsonObject.addProperty("PageIndex", PageIndex);
        jsonObject.addProperty("PageSize", "10");
        jsonObject.addProperty("zoneId", zoneId);
        request.add("request" , jsonObject);
        apiService.getSearch(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            try {
                                JSONObject jsonObj = new JSONObject(responseBody.string());
                                Gson gson = MyGson.create();
                                JResponse objModelSe = gson.fromJson(String.valueOf(jsonObj), JResponse.class);
                                iPersonIFinishedListener.successPerson(objModelSe);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        iPersonIFinishedListener.errorPerson(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });

    }
}
