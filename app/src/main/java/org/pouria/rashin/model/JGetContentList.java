package org.pouria.rashin.model;

public class JGetContentList {
    public String Title;
    public String ThumbImage;
    public String Summary;
    public int ZoneID = -1;
    public int ContentID;
    public boolean FavoriteStatus;

}
