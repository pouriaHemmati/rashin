package org.pouria.rashin.retrofit;





import com.google.gson.JsonObject;

import org.pouria.rashin.utilis.Urls;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    // Search
    @Headers({"Content-Type: application/json"})
    @POST(Urls.SEARCH)
    Observable<ResponseBody> getSearch(@Body JsonObject jsonBody);




}
